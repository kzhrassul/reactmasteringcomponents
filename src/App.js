import logo from './logo.svg';
import './App.css';
import { Fragment } from 'react';
import photos from './photos';
import MyForm from './Forms';

function Header({title = 'Header title'}) {
  return (
    <div className="Header">
      <header className="Header-header">
        <h1 className='Header-header-h1'>{title}</h1>
      </header>
    </div>
  );
}

function Photos(props) {
  return (
    props.photos.slice(0, 10).map((photo) => (
      <img
        key={photo.id}
        className='Image'
        height={220}
        src={photo.url}
        alt={photo.title}
      />
      ))
  )
}

function Main() {
  return (
    <div className='Main'>
      <h1 className='Main-h1'>Photos</h1>
      <div className='Photos'>
        <Photos photos={photos}/>
      </div>
    </div>
  )
}

function Footer({title = 'Footer'}) {
  return (
    <div className='Footer'>
      <footer className='Footer-footer'>{title.toUpperCase()}</footer>
    </div>
  )
}

function App() {
  return (
    <Fragment>
      <Header />
      <Main />
      <MyForm />
      <Footer />
    </Fragment>
  )
}

export default App;
