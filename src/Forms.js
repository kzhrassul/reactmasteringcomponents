import { getValue } from "@testing-library/user-event/dist/utils";
import React from "react";
import { Form, Field } from 'react-final-form'

// const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const handleSubmit = async values => {
  await new Promise(resolve => setTimeout(resolve, 100))
  window.alert(JSON.stringify(values, 0, 2))
}

const required = value => (value ? undefined : 'Field is required')
const mustBeNumber = value => (isNaN(value) ? 'Must be a number' : undefined)
const minValue = min => value =>
  isNaN(value) || value >= min ? undefined : `Should be greater than ${min}`

const composeValidators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined)


const MyForm = () => (
    <Form
      onSubmit={handleSubmit}
      initialValues={{ stooge: 'larry', employed: false }}
      render={({ handleSubmit, form, submitting, pristine, values }) => (
        <form className="Form" onSubmit={handleSubmit}>
          <h1>My form</h1>
          <Field name="firstName" validate={required}>
            {({ input, meta }) => (
              <div className="rows">
                <label>First Name</label>
                <input {...input} type="text" placeholder="First Name" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>
          <Field name="lastName" validate={required}>
            {({ input, meta }) => (
              <div className="rows" >
                <label>Last Name</label>
                <input {...input} type="text" placeholder="Last Name" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <Field
            name="age"
            validate={composeValidators(required, mustBeNumber, minValue(18))}
          >
            {({ input, meta }) => (
              <div className="rows">
                <label>Age</label>
                <input {...input} type="text" placeholder="Age" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>
          <div>
            <label>Employed</label>
            <Field name="employed" component="input" type="checkbox" />
          </div>
          <div className="rows">
            <label>Favorite Color</label>
            <Field name="favoriteColor" component="select">
              <option />
              <option value="Red">Red</option>
              <option value="Green">Green</option>
              <option value="Blue">Blue</option>
            </Field>
          </div>
          <div>
            <label>Sauces</label>
            <div>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="ketchup"
                />{' '}
                Ketchup
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="mustard"
                />{' '}
                Mustard
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="mayonnaise"
                />{' '}
                Mayonnaise
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="guacamole"
                />{' '}
                Guacamole
              </label>
            </div>
          </div>
          <div>
            <label>Best Stooge</label>
            <div>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="Larry"
                />{' '}
                Larry
              </label>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="Moe"
                />{' '}
                Moe
              </label>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="Curly"
                />{' '}
                Curly
              </label>
            </div>
          </div>
          
        <label>Notes</label>
        <Field 
            name="notes"
            component="textarea"
            placeholder="Note"
            rows='4' cols='50'
        />
          
          <div className="btn">
            <button className='btn--submit' type="submit" disabled={submitting || pristine}>
              Submit
            </button>
            <button
              className='btn--reset'
              type="button"
              onClick={form.reset}
              disabled={submitting || pristine}
            >
              Reset
            </button>
          </div>
          <pre className="result">{JSON.stringify(values, 0, 2)}</pre>
        </form>
      )}
    />
)

export default MyForm;